import moment from "moment"
var aspDays =
[
{
	holiday: "New Years Day",
	date: moment("Jan 1, 2018")
},
{
	holiday: "Martin Luther King Jr's Birthday",
	date: moment("Jan 15, 2018")
},
{
	holiday: "Lincoln’s Birthday",
	date: moment("Feb 12, 2018")
},
{
	holiday: "Ash Wednesday",
	date: moment("Feb 14, 2018")
},
{
	holiday: "Asian Lunar New Year",
	date: moment("Feb 16, 2018")
},
{
	holiday: "Washington’s Birthday (Pres. Day)",
	date: moment("Feb 19, 2018")
},
{
	holiday: "Purim",
	date: moment("Mar 1, 2018")
},
{
	holiday: "Holy Thursday",
	date: moment("Mar 29, 2018")
},
{
	holiday: "Good Friday",
	date: moment("Mar 30, 2018")
},
{
	holiday: "Passover",
	date: moment("Mar 31, 2018")
},
{
	holiday: "Passover",
	date: moment("Apr 1, 2018")
},
{
	holiday: "Holy Thursday (Orthodox)",
	date: moment("Apr 5, 2018")
},
{
	holiday: "Good Friday (Orthodox)",
	date: moment("Apr 6, 2018")
},
{
	holiday: "Passover",
	date: moment(" Apr 6, 2018")
},
{
	holiday: "Passover",
	date: moment(" Apr 7, 2018")
},
{
	holiday: "Solemnity of the Ascension",
	date: moment("May 10, 2018")
},
{
	holiday: "Shavuot",
	date: moment("May 20, 2018")
},
{
	holiday: "Shavuot",
	date: moment("May 21, 2018")
},
{
	holiday: "Memorial Day",
	date: moment("May 28, 2018")
},
{
	holiday: "Idul-Fitr",
	date: moment("Jun 15, 2018")
},
{
	holiday: "Idul-Fitr",
	date: moment("Jun 16, 2018")
},
{
	holiday: "Idul-Fitr",
	date: moment("Jun 17, 2018")
},
{
	holiday: "Independence Day",
	date: moment("July 4, 2018")
},
{
	holiday: "Feast of the Assumption",
	date: moment("Aug 15, 2018")
},
{
	holiday: "Idul-Adha",
	date: moment("Aug 21, 2018")
},
{
	holiday: "Idul-Adha",
	date: moment("Aug 22, 2018")
},
{
	holiday: "Idul-Adha",
	date: moment("Aug 23, 2018")
},
{
	holiday: "Labor Day",
	date: moment("Sept 3, 2018")
},
{
	holiday: "Rosh Hashanah",
	date: moment("Sept 10, 2018")
},
{
	holiday: "Rosh Hashanah",
	date: moment("Sept 11, 2018")
},
{
	holiday: "Yom Kippur",
	date: moment("Sept 19, 2018")
},
{
	holiday: "Succoth",
	date: moment("Sept 24, 2018")
},
{
	holiday: "Succoth",
	date: moment("Sept 25, 2018")
},
{
	holiday: "Shemini Atzereth",
	date: moment("Oct 1, 2018")
},
{
	holiday: "Simchas Torah",
	date: moment("Oct 2, 2018")
},
{
	holiday: "Columbus Day",
	date: moment("Oct 8, 2018")
},
{
	holiday: "All Saints Day",
	date: moment("Nov 1, 2018")
},
{
	holiday: "Election Day",
	date: moment("Nov 6, 2018")
},
{
	holiday: "Veterans Day (Observed)",
	date: moment("Nov 12, 2018")
},
{
	holiday: "Thanksgiving Day",
	date: moment("Nov 22, 2018")
},
{
	holiday: "Immaculate Conception",
	date: moment("Dec 8, 2018")
},
{
	holiday: "Christmas Day",
	date: moment("Dec 25, 2018")
}
]
export default aspDays